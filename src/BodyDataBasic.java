/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br></p>
 * 
 * Provides different Indices from a Person with a specified size in cm </br>
 * and a spezified weight in kg. The different indices are rounded (if necessary)</br>
 * to two digits after the point. </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 08.10.2012
 * 
 */
public class BodyDataBasic {

	public enum Gender {MALE, FEMALE};
	
	private final float BSA_WEIGHT_FACTOR = 0.425F;
	private final float BSA_SIZE_FACTOR = 0.725F;
	private final float BSA_M2_KG_CM = 71.84F; 
	private final short ADULT_AGE = 22;
	private final short CHILD_AGE = 17; 
	private final float IBW_MAN_FACTOR = 0.9f; 
	private final float IBW_WOMAN_FACTOR = 0.8f; 
	private final byte ONE_METER_IN_CM = 100; 
		
	/*
	 * Size in cm.
	 */
	private float size;
	/*
	 * weight in kg. 
	 */
	private float weight; 
	
	private Gender gender; 
	
	/**
	 * Provides different Indices from a Person with a specified size in cm
	 * and a spezified weight in kg. The different indices are rounded (if necessary)
	 * to two digits after the point. 
	 * 
	 * @param aSize - size of the Person in cm.
	 * @param aWeight - weight of the Person in gramms.
	 * @throws IllegalArgumentException if a given parameter is lower or equals zero. 
	 */
	public BodyDataBasic (float aSize, int aWeight, Gender aGender  ) 
			throws IllegalArgumentException
	{	
		if (aSize > 0 & aWeight > 0 ) {
			size = aSize;
			weight = aWeight / 1000.0F;
		} else {
			throw new IllegalArgumentException("Input must be greater than 0.");
		}		
		
		gender = aGender; 
	}
	
	/**
	 * Returns the Body Mass Index (BMI) on the base of the size and the weight.
	 * BMI is rounded. 
	 * @return Body Mass Index (BMI)
	 */
	public float getBMI()
	{
		float m = size / 100.0F; //Umwandeln von cm in m. 			
		float bmi = (float) (weight / Math.pow(m, 2));  		// Formel zur Berechung des BMIs 
		return (float) (Math.rint(bmi*10.0)/10.0); 	//Runden auf eine Stelle. 		 
	}
	
	/**
	 * Returns the Body Surface Area (BSA) in m2 for the specified age. 
	 * The Formula used to calculate the BSA is according to Takahira. 
	 * @param age - age of the Person
	 * @return Body Surface Area (BSA) in m2
	 */
	public float getBSA(int age)
	{
		if (age >= ADULT_AGE) {
			return getBSAadults();
		} else if (age < CHILD_AGE){

			return getBSAchildren();
		}
		else {
			return getBSAyoungAdults(); //CHILD < age < ADULT
		}
		
	}

	/**
	 * Returns the Body Surface Area (BSA) in m2 for adults (age >= 22 years).
	 * @return Body Surface Area (BSA) in m2
	 */
	public float getBSAadults()
	{
		float bsa = (float) ((Math.pow(weight, BSA_WEIGHT_FACTOR) 
				* Math.pow(size, BSA_SIZE_FACTOR) 
				* BSA_M2_KG_CM)/10000.0F);
		
		return (float) (Math.rint(bsa*100.0)/100.0); 	//Runden auf zwei Stellen. 
	}
	
	/**
	 * Returns the Body Surface Area (BSA) in m2 for children (age < 17 years).
	 * @return Body Surface Area (BSA) in m2
	 */
	public float getBSAchildren()
	{
		float bsa = (float) Math.sqrt((weight * size)/3600.0); 
		return (float) (Math.rint(bsa*100.0)/100.0); 	//Runden auf zwei Stellen.		
	}
	
	/**
	 * Returns the Body Surface Area (BSA) in m2 for 
	 * young adults (17 <= age < 22 years).
	 * @return Body Surface Area (BSA) in m2
	 */
	public float getBSAyoungAdults() 
	{
		float bsa = (getBSAchildren() + getBSAadults())/2; 		
		return (float) (Math.rint(bsa*100.0)/100.0); 	//Runden auf zwei Stellen.
	}
	
	/**
	 * Returns the Ideal Body Weight (IBW) in kg (rounded), 
	 * for the Gender defined in on creating the Object,
	 * according to the Broca index on the base of the size.
	 * 
	 * @return Ideal Body Weight (IBW) in kg (rounded) 
	 */
	public int getIBW()
	{
		if (gender == Gender.MALE) {
			return getIBWmen();
		} else {
			return getIBWwomen();
		}
	}
	
	/**
	 * Returns the Ideal Body Weight (IBW) in kg (rounded) 
	 * for men according to the Broca index on the base of the size.
	 * @return Ideal Body Weight (IBW) in kg (rounded) 
	 */
	public int getIBWmen()
	{		
		float ibw = (float)((size - ONE_METER_IN_CM) * IBW_MAN_FACTOR); 	
		return Math.round(ibw); 
	}
	
	/**
	 * Returns the Ideal Body Weight (IBW) in kg (rounded) 
	 * for women according to the Broca index on the base of the size.
	 * @return Ideal Body Weight (IBW) in kg (rounded) 
	 */
	public int getIBWwomen()
	{
		float ibw = (float)((size - ONE_METER_IN_CM) * IBW_WOMAN_FACTOR); 	
		return Math.round(ibw); 
	}
	
	/**
	 * Returns the Normal Body Weight (NBW) in kg according to the 
	 * Broca index on the base of the size.
	 * @return Normal Body Weight (NBW) in kg
	 */
	public int getNBW()
	{	
		short cm = (short)size; 
		return cm-ONE_METER_IN_CM; 
	}	
}
