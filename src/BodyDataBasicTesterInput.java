import java.util.Scanner;



/**
 * Berner Fachhochschule</p>
 * Medizininformatik BSc</p>
 * Modul 8051-HS2012</p>
 * 
 * Enables an User to calculate different Indices 
 * based on his size, weight and age. 
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class BodyDataBasicTesterInput {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		BodyDataBasic.Gender gender; 
		
		Scanner in = new Scanner(System.in);
		
		if (in != null) {
			System.out.print("Geben Sie ihr Gewicht in gramm ein: ");
			int weight = in.nextInt();
			
			System.out.print("Geben Sie ihre Koerpergroesse in cm ein: ");
			float size = in.nextFloat();
				
			System.out.print("Geben Sie ihr Alter in Jahren  ein: ");
			int age = in.nextInt();
			
			System.out.print("Geben Sie ihr Geschlecht ein (m/f): ");
			String genderInput = in.next();
			if (genderInput.equalsIgnoreCase("m")) {
				gender = BodyDataBasic.Gender.MALE; 
			} else { 
				gender = BodyDataBasic.Gender.FEMALE;
			}
			
			in.close(); 
			
			BodyDataBasic bdcNew = new BodyDataBasic(size, weight, gender);
			
			System.out.println("\nAus ihren Angaben erfolgt: "); 
			System.out.println("BMI: " + bdcNew.getBMI()); 	
			System.out.println("Body Surface Area (BSA): " + bdcNew.getBSA(age) + " m2"); 	
			System.out.println("Ideal Body Weight (IBW): " + bdcNew.getIBWmen() + " kg");
			System.out.println("Normal Body Weight (NBW): " + bdcNew.getNBW() + " kg"); 
		}
		
		
	}

}
